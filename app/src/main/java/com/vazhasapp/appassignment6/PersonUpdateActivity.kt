package com.vazhasapp.appassignment6

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.widget.DatePicker
import com.vazhasapp.appassignment6.databinding.ActivityMainBinding
import com.vazhasapp.appassignment6.databinding.ActivityPersonUpdateBinding
import java.util.*

class PersonUpdateActivity : AppCompatActivity(), DatePickerDialog.OnDateSetListener {

    private lateinit var binding: ActivityPersonUpdateBinding

    private var dayOfMonth = 0
    private var month = 0
    private var year = 0

    private var savedDayOfMonth = 0
    private var savedMonth = 0
    private var savedYear = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPersonUpdateBinding.inflate(layoutInflater)
        setTheme(R.style.Theme_APPAssignment6)
        setContentView(binding.root)

        init()

    }

    private fun init() {
        binding.btnDateOfBirthPicker.setOnClickListener {
            pickDate()
        }

        binding.btnSaveUpdatedInfo.setOnClickListener {
            intent.putExtra(MainActivity.MY_INTENT_VALUE_NAME, getUpdatedPersonInfo())
            setResult(RESULT_OK, intent)
            finish()
        }
    }

    private fun getUpdatedPersonInfo(): Person {
        val updatedFirstName = binding.etUpdatedFirstName.text.toString()
        val updatedLastName = binding.etUpdatedLastName.text.toString()
        val updatedEmail = binding.etUpdatedEmail.text.toString()
        val updatedDateOfBirth = binding.btnDateOfBirthPicker.text.toString()
        val updatedSex = getSexOfUpdatedPerson()

        return Person(
            updatedFirstName,
            updatedLastName,
            updatedEmail,
            updatedDateOfBirth,
            updatedSex,
        )
    }

    private fun getSexOfUpdatedPerson(): String {
        return if (binding.rdMale.isChecked)
            "Male"
        else
            "Female"
    }

    private fun getCurrentDate() {
        val calendar = Calendar.getInstance()
        dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH)
        month = calendar.get(Calendar.MONTH)
        year = calendar.get(Calendar.YEAR)
    }

    private fun pickDate() {
        getCurrentDate()
        DatePickerDialog(this, this, year, month, dayOfMonth).show()
    }

    @SuppressLint("SetTextI18n")
    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        savedDayOfMonth = dayOfMonth
        savedMonth = month
        savedYear = year

        binding.btnDateOfBirthPicker.text = "$dayOfMonth/$month/$year"
    }
}