package com.vazhasapp.appassignment6

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Person(
    val firstName: String,
    val lastName: String,
    val email: String,
    val dateOfBirth: String,
    val sex: String,
): Parcelable
