package com.vazhasapp.appassignment6

import android.app.Activity
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.widget.DatePicker
import android.widget.Toast
import com.vazhasapp.appassignment6.databinding.ActivityMainBinding
import java.util.*

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setTheme(R.style.Theme_APPAssignment6)
        setContentView(binding.root)

        init()

    }

    private fun init() {

        binding.btnUpdateProfile.setOnClickListener {
            val myIntent = Intent(this, PersonUpdateActivity::class.java)

            startActivityForResult(myIntent, MY_REQUEST_CODE)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == MY_REQUEST_CODE && resultCode == RESULT_OK) {
            val updatedUser = data?.getParcelableExtra<Person>(MY_INTENT_VALUE_NAME)
            setUpdatedUserOnScreen(updatedUser!!)
        }
    }

    private fun setUpdatedUserOnScreen(updatedUser: Person) {
        binding.apply {
            tvFirstName.text = updatedUser.firstName
            tvLastName.text = updatedUser.lastName
            tvEmail.text = updatedUser.email
            btnDateOfBirthPicker.text = updatedUser.dateOfBirth
            rdGroup.check(if (updatedUser.sex == "Male") R.id.rdMale else R.id.rdFemale)
        }
    }

    companion object {
        const val MY_REQUEST_CODE = 1312
        const val MY_INTENT_VALUE_NAME = "UpdatedPerson"
    }
}